package org.fasttrackit;

public class DemoShop {
    public static void main(String[] args) {
        System.out.println(" - Demo Shop - ");
        System.out.println("User can login with valid credentials.");
        Page page = new Page();
        page.openHomePage();
        Header header = new Header();
        header.clickOnTheLoginButton();
        ModalDialog modal = new ModalDialog();
        modal.typeInUsername("dino"); //passing in an argument
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        String greetingsMessage = header.getGreetingsMessage();
        boolean isLogged = greetingsMessage.contains("dino");
        System.out.println(" Hi Dino is displayed in the header:" + isLogged);
        System.out.println("Greetings msg is:" + greetingsMessage);

        System.out.println("------------------");
        System.out.println("2. User can add product to cart from product cards");
        page.openHomePage();
        ProductCards cards = new ProductCards();
        Product awesomeGraniteChips = cards.getProductByName("Awesome Granite Chips");
        System.out.println("Product is:" + awesomeGraniteChips);
        awesomeGraniteChips.clickOnTheProductCartIcon();

        System.out.println("------------------");
        System.out.println("3.User can navigate to Home Page from Whishlist Page");
        page.openHomePage();
        header.clickOnTheWhishlistIcon();
        String pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Wishlist page. Title is: " + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home page. Title is: " + pageTitle);

        System.out.println("------------------");
        System.out.println("4.User can navigate to Home Page from Cart Page");
        page.openHomePage();
        header.clickOnTheCartIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Cart page. Title is :" + pageTitle);
        header.clickOnTheShoppingBagIcon();
        pageTitle = page.getPageTitle();
        System.out.println("Expected to be on the Home page. Title is :" + pageTitle);
    }
}

// test case 3
//Title: user must be able to navigate to Home Page from Whishlist Page
//1.Open  Home Page
//2.Click on the  "favorites icon"
//3.click on the "Shopping bag"
//4.Expected: Home Page is loaded


// test case 2
//Title : User can add product to cart from product cards
//1. Open  Home Page. (- method or function - class)
//2.Click on the product item in cart


// test case 1
//Title: Login Button
//1. Open  Home Page. (- method or function - class)
// 2. Click on the login button.
// 3. Click on the Username Field.
// 4. Type in dino.
// 5. Click on the Password Field.
// 6. Type in choochoo.
// 7. Click on the login button.
// Expected: Hi dino is displayed in the header.
